
package com.example.matrixcalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {


    private float num1;
    private float num2;
    private float num3;
    private float num4;
    private float num5;
    private float num6;
    private float num7;
    private float num8;
    private float num9;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /**
         *  Gets the button and changes a textView component
         * */
        Button testBtn = (Button) findViewById(R.id.detBtn);
        testBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                TextView editText = findViewById(R.id.operationTitle);
                editText.setText("Determinant is: " + calculateDeterminant(getData())); //Changes the text of the operation
            }
        });

        Button invBtn = findViewById(R.id.inverseBtn);
        invBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                calculateInverse(getData());
            }
        });

        Button transposeBtn = findViewById(R.id.transposeBtn);
        transposeBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                transposeMatrix(getData());
            }
        });
    }

    /**
     *  Calculates the determinant of a given matrix using matrix determinant's math properties
     * */
    public float calculateDeterminant(float... num) {

        float res1 = 0;
        float res2 = 0;
        float res3 = 0;
        float result = 0;


        res1 = num[0]*((num[4]*num[8]) - (num[7]*num[5]));
        res2 = num[1]*((num[3]*num[8]) - (num[6]*num[5]));
        res3 = num[2]*((num[3]*num[7]) - (num[6]*num[4]));

        System.out.println("Result_1 was: " + res1);
        System.out.println("Result_2 was: " + res2);
        System.out.println("Result_3 was: " + res3);

        result = (res1 - res2 + res3);
        System.out.println("Final result is: " + result);

        return result;

    }

    /**
     *  Calculates the inverse of a given matrix if the determinant is not 0. It uses the formula
     *
     *   Inv = (1/det)*[transposed_matrix]
     * */
    public void calculateInverse(float... args) {

        float[][] matrix = new float[3][3];
        float determinant = 0;


        int count = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                matrix[i][j] = args[count]; //Load matrix
                //System.out.println("Count is: " + count + " arg is: " + args[count]);
                count++;
            }
        }

        determinant = calculateDeterminant(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]);

        if (determinant == 0)
        {
            System.out.println("[ERROR] Determinant is 0, there is no inverse!");
            TextView textView = findViewById(R.id.operationTitle);
            textView.setText("[ERROR] determinant is 0!");
            return;
        }

        int count_2 = 0;
        float[] result = new float[9];


        //Nota para el profe: Podria haber utilizado la función de 'transposeMatrix' para ahorrarmelo, viene a ser el mismo metodo

        System.out.println("Inverse is: ");
        for(int i = 0; i < 3; i++) {
            for(int j = 0; j < 3; j++) {
                System.out.print("[ " + count_2 + " ]");
                result[count_2] = (((matrix[(j+1)%3][(i+1)%3] * matrix[(j+2)%3][(i+2)%3]) - (matrix[(j+1)%3][(i+2)%3] * matrix[(j+2)%3][(i+1)%3]))/ determinant);
                System.out.print(result[count_2] + "\n");
                count_2++;
            }
        }

        setData(result);

        TextView textView = findViewById(R.id.operationTitle);
        textView.setText("Inverse Matrix Solution");
    }

    /**
     *  Calculates the transpose of a given matrix, could be used as input for the previous inverse function,
     *  but it's something anyone with more spare time or lack of social life could do.     *
     */
    public void transposeMatrix(float... args) {

        float[][] matrix = new float[3][3];

        int count = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                matrix[i][j] = args[count]; //Load matrix
                //System.out.println("Count is: " + count + " arg is: " + args[count]);
                count++;
            }
        }

        float[][] tmp = new float[3][3];


        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                 tmp[j][i] = matrix[i][j];

            }
        }

        float[] result = new float[9];
        int count_2 = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                result[count_2] = tmp[i][j];
                count_2++;
            }
        }

        setData(result);

        TextView textView = findViewById(R.id.operationTitle);
        textView.setText("Transpose Matrix Solution");

    }

    /**
     *  Gets the data from each slot shown in the UI and outputs an array of floats which then are given as param
     *  to the previous functions
     */
    public float[] getData() {

        float[] data = new float[9];

        try {
            EditText slot1 = findViewById(R.id.slot1);
            data[0] = Float.parseFloat(slot1.getText().toString());

            EditText slot2 = findViewById(R.id.slot2);
            data[1] = Float.parseFloat(slot2.getText().toString());

            EditText slot3 = findViewById(R.id.slot3);
            data[2] = Float.parseFloat(slot3.getText().toString());

            EditText slot4 = findViewById(R.id.slot4);
            data[3] = Float.parseFloat(slot4.getText().toString());

            EditText slot5 = findViewById(R.id.slot5);
            data[4] = Float.parseFloat(slot5.getText().toString());

            EditText slot6 = findViewById(R.id.slot6);
            data[5] = Float.parseFloat(slot6.getText().toString());

            EditText slot7 = findViewById(R.id.slot7);
            data[6] = Float.parseFloat(slot7.getText().toString());

            EditText slot8 = findViewById(R.id.slot8);
            data[7] = Float.parseFloat(slot8.getText().toString());

            EditText slot9 = findViewById(R.id.slot9);
            data[8] = Float.parseFloat(slot9.getText().toString());
        } catch (Exception e)
        {
            System.out.println("[ERROR] There was an unexpected error");
        }

        return data;
    }

    /**
     * Sets the data from the result of a function into the display, used along with
     * /*TODO another function to output [ERROR], [INFO] and [WARN] Strings for the user to understand and address the an error correctly
     *          instead of smashing the machine to pieces or crying in a dark corner.
     */
    public void setData(float... args) {

        EditText slot1 = findViewById(R.id.slot1);
        slot1.setText(String.valueOf(args[0]));

        EditText slot2 = findViewById(R.id.slot2);
        slot2.setText(String.valueOf(args[1]));

        EditText slot3 = findViewById(R.id.slot3);
        slot3.setText(String.valueOf(args[2]));

        EditText slot4 = findViewById(R.id.slot4);
        slot4.setText(String.valueOf(args[3]));

        EditText slot5 = findViewById(R.id.slot5);
        slot5.setText(String.valueOf(args[4]));

        EditText slot6 = findViewById(R.id.slot6);
        slot6.setText(String.valueOf(args[5]));

        EditText slot7 = findViewById(R.id.slot7);
        slot7.setText(String.valueOf(args[6]));

        EditText slot8 = findViewById(R.id.slot8);
        slot8.setText(String.valueOf(args[7]));

        EditText slot9 = findViewById(R.id.slot9);
        slot9.setText(String.valueOf(args[8]));
    }
}
